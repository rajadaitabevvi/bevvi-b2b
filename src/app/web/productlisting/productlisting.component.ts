import { Directive } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, ElementRef, NgZone, OnInit, ViewChild, HostListener } from '@angular/core';
import { ProviderServiceService } from "../../../app/shared/services/provider-service.service";
import { FormControl } from '@angular/forms';
import { UserService } from '../../shared/services/user.service';
import { MapsAPILoader } from '@agm/core';
import * as $ from "jquery";

@Component({
    selector: 'app-productlisting',
    templateUrl: './productlisting.component.html',
    styleUrls: ['./productlisting.component.css']
})

export class ProductlistingComponent implements OnInit {
    // @HostListener("window:scroll", [])
    // onWindowScroll() {
    //     this.scrollPosition = (window.pageYOffset);
    //     console.log()
    // }



    public isUserLoggedIn: boolean = this.userService.isLoggedIn();
    public productListing: Array<any> = [];
    public filterListing: Array<any> = [];
    public allProductListing: Array<any> = [];
    public tempProductListing: Array<any> = [];
    public productFilterArray: Array<any> = [];
    public currentUserDetails: any = (this.userService.isLoggedIn()) ? this.userService.getCurrentUserData() : {};
    public userDeliveryAddress: Array<any> = (this.userService.getuserDeliveryLocation()) ? this.userService.getuserDeliveryLocation() : [];
    public userDeliveryAddressToUse: Object = (this.userService.getuserDeliveryLocationToUse()) ? this.userService.getuserDeliveryLocationToUse() : ((this.userService.getuserDeliveryLocation() && this.userService.getuserDeliveryLocation().length > 0) ? this.userService.getuserDeliveryLocation()[0] : {});
    public userDeliveryAddressToUseAddressId: string = "";
    public userTravelMode: string = (this.userService.getUserTransportmode() == 0) ? "walk" : "drive";
    // public radius: number = this.userService.getUserRadiusMiles();
    //public tmpRadius: number = this.userService.getUserRadiusMiles();;
    public selectedEstablishmentId: string = "";
    public breadcumbEstablishmentId: string = "";
    public breadcumbProductCategory: string = "ALL";
    public productCount: any = -1;
    public productCountDisplay: any = 0;
    // public searchOpen: boolean = false;
    public searchIntervalID: any = 0
    public isError: boolean = false;
    public latitude: number;
    public longitude: number;
    public formatted_address: string;
    public searchControl: FormControl;
    public deliveryAddress: string = "";
    // public isListActive:boolean = false;
    public classObject = [];
    public sortingKey = 'product.iName';
    public sortingReverse = false;
    public sortingTypeSelected = 'Name: A-Z';
    public offerloaded: boolean = false;
    public scrollPosition = 0;
    public storeState = {};
    public allDataListWithFilters = {};
    public selectedFilter = 'category';
    public searchCount = 1;
    public record_Open_Azcordian = [];
    public sortingApplied = false;

    @ViewChild("delivery") searchElementRef: ElementRef;


    constructor(
        private userService: UserService,
        private router: Router,
        private ProviderServiceService: ProviderServiceService,
        private route: ActivatedRoute,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone
    ) {

    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params.establishmentId) {
                this.breadcumbEstablishmentId = params.establishmentId;
            }
            if (params.productCategory) {
                this.breadcumbProductCategory = params.productCategory;
            }
        });

        console.log("offerLoaded " + this.userService.getOfferLoaded());
        this.searchControl = new FormControl();
        this.mapsAPILoader.load().then(() => {
            console.log("hey");
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["address"]
            });
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    this.isError = false;
                    // this.isListActive = true;
                    console.log(place);
                    //this.searchOpen = true;
                    if (place.geometry === undefined || place.geometry === null) {
                        this.isError = true;
                        return;
                    }
                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.formatted_address = place.formatted_address

                    let userPickupAddress = [{
                        "geoLocation": {
                            "coordinates": [
                                place.geometry.location.lng(),
                                place.geometry.location.lat()
                            ],
                            "type": "Point"
                        },
                        "address": this.formatted_address
                    }];
                });
            });
        });
        //this.getAllProducts('');
        this.userService.showAppSpinner();
        //this.classObject = this.userService.getFilterObj();
        this.userDeliveryAddressToUse = (this.userService.getuserDeliveryLocationToUse()) ? this.userService.getuserDeliveryLocationToUse() : ((this.userService.getuserDeliveryLocation() && this.userService.getuserDeliveryLocation().length > 0) ? this.userService.getuserDeliveryLocation()[0] : {});
        if (Object.keys(this.userDeliveryAddressToUse).length === 0) {
            this.userService.setIsLocation(false);
        } else {
            this.userService.setIsLocation(true);
        }

        this.userService.setAnimationToCategoryFilter($('#accord1'));

        this.ngZone.run(() => { // <== added
            this.storeState = this.userService.getStoreState();
            if (Object.keys(this.storeState).length != 0 && this.userService.getPreviousUrl().indexOf('productdetail') > -1) {
                this.setAllValues(this.storeState);

            }
            else {
                this.userService.isSerachTerm.subscribe(searchText => {
                    this.getAllProducts(searchText);
                });
            }

        });
        console.log(this.userDeliveryAddressToUse);
    }

    showImages(i){
       
            this.productListing[i].loaded = true;
               
      
    }

    getAllProducts(searchText: any) {
        this.classObject = [];
        $(".all").prop("checked", true);
        if (Object.keys(this.userDeliveryAddressToUse).length > 0) {
            this.ProviderServiceService.getCurrentOffers(this.userDeliveryAddressToUse, searchText)
                .subscribe(data => {
                    console.log(data);
                    data.offers.forEach(element => {
                        element.loaded = false;
                      });
                    this.allDataListWithFilters = data;
                    this.userService.offerLoaded = true;
                    //this.userService.setFilterObj() data.filters;

                    this.allProductListing = this.productListing = this.tempProductListing = data.offers;
                    this.filterListing = data.filters;


                    this.productCount = this.productCountDisplay = this.productListing.length;
                    this.sortItems(this.sortingKey, this.sortingReverse);
                    console.log(this.productListing);
                });
        } else {
            this.router.navigate(['/']);
        }
    }
    applyClassFilter(j) {
        for (var f = 0; f < this.classObject.length; f++) {
            if (this.classObject[f]['catIndex'] == j) {
                return true;
            }
        }
        return false;
    }
    applyClassSubFilter(j, i) {
        for (var f = 0; f < this.classObject.length; f++) {
            if (this.classObject[f]['catIndex'] == j) {
                if (this.classObject[f]['valueIndex'] == i) {
                    return true;
                }
            }
        }
        return false;
    }
    recordOpenaAcordian(j, ev) {
        if (ev.target.nodeName == 'SPAN' || ev.target.nodeName == "IMG") {
            if (this.record_Open_Azcordian.includes(j)) {
                this.record_Open_Azcordian.splice(this.record_Open_Azcordian.indexOf(j), 1);
            }
            else {
                this.record_Open_Azcordian.push(j);
            }
        }
        console.log(this.record_Open_Azcordian);

    }

    // doSomething(){
    //     console.log('hey');
    // }
    fliterProductCategoary(categoryType, value, catIndex, valueIndex) {
        //alert('hey');
        // let tempListing = this.productListing;
        // this.productListing = [];
        // tempListing.forEach(element => {
        //     element.loaded = false;
        //     this.productListing.push(element);
        //   });
         // this.productListing = tempListing;
        if (categoryType == 'price') {
            value = value.trim();
            if (value.indexOf('<') != -1) {
                value = value.split('<');
                value[0] = 0;
                value[1] = +(value[1].substring(1, value[1].length - 1));
            } else if (value.indexOf('+') != -1) {
                value = value.split('$');
                console.log(value);
                value[0] = +(value[0].substring(0, value[0].length));
                value[1] = 100000;
            }
            else {
                value = value.split('-');
                value[0] = +(value[0].substring(0, value[0].length - 2));
                value[1] = +(value[1].substring(1, value[1].length - 1));
            }
        }
        console.log(value);
        var tempObj = {
            'catIndex': catIndex,
            'valueIndex': valueIndex,
            'cat': categoryType,
            'value': value
        }
        console.log(tempObj);
        let isFound = false;
        this.productListing = this.tempProductListing;
        //this.productListing = this.allProductListing;
        console.log(this.productListing);
        if (this.classObject.length == 0) {
            this.classObject.push(tempObj);
        }
        else {
            for (var f = 0; f < this.classObject.length; f++) {
                console.log(this.classObject[f]['catIndex']);
                if (this.classObject[f]['catIndex'] == catIndex) {
                    if (this.classObject[f]['valueIndex'] == valueIndex) {
                        this.classObject.splice(f, 1);
                        isFound = true;
                        break;
                    }
                    this.classObject[f] = tempObj;
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                this.classObject.push(tempObj);
            }
        }
        console.log(this.tempProductListing.length);
        if (this.classObject.length < 1) {
            this.productListing = this.tempProductListing;
        } else {
            for (var g = 0; g < this.classObject.length; g++) {
                if (this.classObject[g].cat == 'price') {
                    this.productListing = this.productListing.filter(
                        productdetail => productdetail['salePrice'] >= this.classObject[g].value[0] && productdetail['salePrice'] <= this.classObject[g].value[1]);

                } else if (this.classObject[g].cat == 'size') {
                    this.productListing = this.productListing.filter(
                        productdetail => productdetail.product['size'].toString() + ' ' + productdetail.product['units'].toString() == this.classObject[g].value);
                }

                else {
                    let tempListing = this.productListing;
                    this.productListing = [];
                    for (var k = 0; k < tempListing.length; k++) {
                        //console.log(tempListing[k].product[this.classObject[g].cat])
                        if (tempListing[k].product[this.classObject[g].cat]) {
                            console.log(tempListing[k].product[this.classObject[g].cat]);
                            console.log(this.classObject[g].value);
                            if (tempListing[k].product[this.classObject[g].cat].toString().includes(this.classObject[g].value.toString())) {
                                console.log(tempListing[k].product[this.classObject[g].cat]);
                                console.log(this.classObject[g].value);
                                this.productListing.push(tempListing[k]);
                            }
                        }
                    }

                    console.log(this.productListing);
                    // this.productListing = this.productListing.filter(
                    //     productdetail => productdetail.product[this.classObject[g].cat] ==(this.classObject[g].value));
                }
            }


        }
        this.productCount = this.productCountDisplay = this.productListing.length;
        this.userService.setFilterObj(this.classObject);
    }
    sortItems(key, type) {

        if (key == 'product.iName' && type == false) {
            for (var i = 0; i < this.productListing.length; i++) {
                if (this.productListing[i].isSponsored) {
                    this.productListing[i].product['iName'] = '0' + this.productListing[i].product['name'];
                } else {
                    this.productListing[i].product['iName'] = this.productListing[i].product['name'];
                }
            }
        }
        if (key == 'product.iName' && type == true) {
            for (var i = 0; i < this.productListing.length; i++) {
                if (this.productListing[i].isSponsored) {
                    this.productListing[i].product['iName'] = 'ZZ' + this.productListing[i].product['name'];
                    console.log(this.productListing[i].product['iName']);

                }
            }
        }
        if (key == 'iSalePrice' && type == false) {
            for (var i = 0; i < this.productListing.length; i++) {
                if (this.productListing[i].isSponsored) {
                    this.productListing[i].iSalePrice = this.productListing[i].salePrice - 10000;
                    console.log(this.productListing[i].iSalePrice);
                    console.log(this.productListing[i].salePrice);
                } else {
                    this.productListing[i].iSalePrice = this.productListing[i].salePrice;
                }
            }
        }
        if (key == 'iSalePrice' && type == true) {
            for (var i = 0; i < this.productListing.length; i++) {
                if (this.productListing[i].isSponsored) {
                    this.productListing[i].iSalePrice = this.productListing[i].salePrice + 10000;
                    console.log(this.productListing[i].iSalePrice);
                    console.log(this.productListing[i].salePrice);
                } else {
                    this.productListing[i].iSalePrice = this.productListing[i].salePrice;
                }
            }
        }


    }

    sortProduct(key, type, ev) {
        console.log(ev.target.text);
        console.log(key, type, ev);
        this.sortItems(key, type);
        console.log(this.productListing)
        this.sortingKey = key;
        this.sortingReverse = type;
        this.sortingTypeSelected = ev.target.text;
        this.sortingApplied = true;
    }

    public categoryFilter(args) {
        //alert('hey');
        // let tempListing = this.productListing;
        // this.productListing = [];
        // tempListing.forEach(element => {
        //     element.loaded = false;
        //     this.productListing.push(element);
        //   });
         // this.productListing = tempListing;
        this.classObject = [];
        this.breadcumbProductCategory = args.target.value;
        this.record_Open_Azcordian = [];
        console.log(args);
        console.log(this.allDataListWithFilters);

        this.filterListing = this.allDataListWithFilters[args.target.value.toLowerCase() + 'Filters'];
        if (args.target.value == 'Mixer') {
            this.filterListing = this.allDataListWithFilters['mixersFilters'];
        }
        console.log(this.filterListing);
        if (args.target.value == 'All') {
            this.selectedFilter = 'allCategory';
            this.filterListing = this.allDataListWithFilters['filters'];
            this.productListing = this.allProductListing;
            this.tempProductListing = this.productListing;
            this.productCount = this.productCountDisplay = this.productListing.length;
            this.sortItems(this.sortingKey, this.sortingReverse);
            return;
        }
        if (args !== true) {
            this.selectedFilter = 'subCategory';
            this.productListing = this.allProductListing;
            this.productFilterArray = [];
            $("input[name='categoryFilter']").each((index, item) => {
                if ($(item).is(":checked")) {
                    $(item).prop('checked', false);
                }
            });
            $(args.currentTarget).prop('checked', true);
            this.productFilterArray.push($(args.currentTarget).val());
        }


        if (this.productFilterArray.length > 0) {
            this.productListing = this.productListing.filter(
                product => {
                    return ($.inArray(product.product.category, this.productFilterArray) > -1) ? true : false;
                }
            );

            this.tempProductListing = this.productListing;
            this.productCount = this.productCountDisplay = this.productListing.length;
        }
        this.sortItems(this.sortingKey, this.sortingReverse);
    }

    // myOnChange(args) {
    //     this.tmpRadius = args.from;
    //     return false;
    // }

    showProductListingOnMobile() {
        $('#stores ul li').click(function () {
            $('.mobileview-productlisting').css(
                {
                    'transform': 'translate(0%)',
                    'transition': 'all linear 0.2s'
                }
            );
        });
        $('.close-productListing-btn').click(function () {
            $('.mobileview-productlisting').css(
                { 'transform': 'translate(100%)' }
            );
        });
    }

    // openSearch() {
    //     $("#addressDropDown").toggle();
    //  }
    // closeSearch() {
    //     console.log('hey');
    //     setTimeout(() => {
    //         if(this.isListActive){
    //             this.isListActive = false;
    //             return;
    //         }
    //         this.searchOpen = false;
    //         this.isListActive = false;
    //     }, 500);
    // }
    saveUserDeliveryLocation() {

        // if (this.userService.isLoggedIn()) {
        //     this.ProviderServiceService.updateUserProfile(this.tmpRadius, this.userService.getUserTransportmode())
        //         .subscribe(data => {
        //             this.ProviderServiceService.getAccountData(this.userService.getCurrentAccountId())
        //                 .subscribe(data => {
        //                     this.radius = this.tmpRadius;
        //                     this.userService.setCurrentUserData(data);
        //                 })
        //         })
        // }
        //this.isListActive = true;
        console.log(this.latitude + " " + this.longitude);
        if (this.latitude == null || this.longitude == null) {
            console.log('hey');
            this.isError = true;
            //this.searchOpen = true;
            this.latitude = null;
            this.userService.hideAppSpinner();
            //$('.pickupheader.profile-head .dropdown-toggle').attr('aria-expanded', "false").parent().removeClass('open');
        } else {
            this.isError = false;
            let userPickupAddress = [{
                "geoLocation": {
                    "coordinates": [
                        this.longitude,
                        this.latitude
                    ],
                    "type": "Point"
                },
                "address": this.formatted_address
            }];
            if (this.userService.isLoggedIn()) {
                this.ProviderServiceService.addLocation(this.latitude, this.longitude)
                    .subscribe(data => {
                        this.ProviderServiceService.addUserDeliveryLocation(this.latitude, this.longitude, this.formatted_address, data['id'])
                            .subscribe(data => {
                                this.ProviderServiceService.getuserDeliveryLocation()
                                    .subscribe(data => {
                                        //this.radius = this.tmpRadius;
                                        this.userService.setuserDeliveryLocation(data);
                                        this.userService.setuserDeliveryLocationToUse(data[0]);
                                        this.userDeliveryAddressToUse = data[0];
                                        this.userDeliveryAddress = data;
                                        this.userDeliveryAddressToUseAddressId = this.userDeliveryAddressToUse['id'];
                                        // $(".history li").eq(0).trigger("click");
                                        $("input[name='delivery']").val('');
                                        $("body").trigger("click");
                                        this.userService.hideAppSpinner();
                                        this.userService.setHeaderCartCheck(true);
                                        //this.searchOpen = false;
                                        this.latitude = null;
                                        console.log(this.latitude);
                                        this.getAllProducts("");
                                    });
                            },
                                error => {
                                    this.userService.showflashMessage("danger", "Address already exists in your account. Please select the same and continue.");
                                    this.userService.hideAppSpinner();
                                });
                    },
                        error => {
                            this.userService.showflashMessage("danger", "Address already exists in your account. Please select the same and continue.");
                            this.userService.hideAppSpinner();
                        });
            } else {
                //this.radius = this.tmpRadius;
                this.userDeliveryAddress = userPickupAddress;
                this.userDeliveryAddressToUse = userPickupAddress[0];
                this.userDeliveryAddressToUseAddressId = this.userDeliveryAddressToUse['id'];
                $("input[name='delivery']").val('');
                $("body").trigger("click");
                this.userService.setuserDeliveryLocation(userPickupAddress);
                this.userService.setuserDeliveryLocationToUse(userPickupAddress[0]);
                this.userService.hideAppSpinner();
                this.userService.setHeaderCartCheck(true);
                this.latitude = null;
                console.log(this.latitude);
                this.getAllProducts("");
                //this.searchOpen = false;

            }
        }
    }
    ngOnDestroy() {
        this.userService.setProductListing(this.productListing, this.allProductListing);
        this.userService.setFilterListing(this.filterListing);
        this.userService.setOfferLoaded(true);
        this.storeState['isError'] = this.isError;
        this.storeState['productListing'] = this.productListing;
        this.storeState['filterListing'] = this.filterListing;
        this.storeState['allProductListing'] = this.allProductListing;
        this.storeState['tempProductListing'] = this.tempProductListing;
        this.storeState['productFilterArray'] = this.productFilterArray;
        this.storeState['breadcumbProductCategory'] = this.breadcumbProductCategory;
        this.storeState['latitude'] = this.latitude;
        this.storeState['longitude'] = this.longitude;
        this.storeState['formatted_address'] = this.formatted_address;
        this.storeState['classObject'] = this.classObject;
        this.storeState['sortingKey'] = this.sortingKey;
        this.storeState['sortingReverse'] = this.sortingReverse;
        this.storeState['sortingTypeSelected'] = this.sortingTypeSelected;
        this.storeState['productCount'] = this.productCountDisplay = this.productCount;
        console.log(this.record_Open_Azcordian);
        this.storeState['scrollPosition'] = this.scrollPosition;
        this.storeState['allDataListWithFilters'] = this.allDataListWithFilters;
        this.storeState['record_Open_Azcordian'] = this.record_Open_Azcordian;
        //console.log("Scroll Position  "+ document.body.scrollTop);

    }
    setAllValues(obj) {
        console.log(this.storeState)
        this.isError = this.storeState['isError'];
        this.productListing = this.storeState['productListing'];
        this.productListing.forEach(element => {
            element.loaded = false;
          });
        this.filterListing = this.storeState['filterListing'];
        this.allProductListing = this.storeState['allProductListing'];
        this.tempProductListing = this.storeState['tempProductListing'];
        this.productFilterArray = this.storeState['productFilterArray'];
        this.breadcumbProductCategory = this.storeState['breadcumbProductCategory'];
        this.latitude = this.storeState['latitude'];
        this.longitude = this.storeState['longitude'];
        this.formatted_address = this.storeState['formatted_address'];
        this.classObject = this.storeState['classObject'];
        this.sortingKey = this.storeState['sortingKey'];
        this.sortingReverse = this.storeState['sortingReverse'];
        this.sortingTypeSelected = this.storeState['sortingTypeSelected'];
        this.productCount = this.productCountDisplay = this.storeState['productCount'];
        //console.log("Scroll Position  " + document.body.scrollTop);
        this.scrollPosition = this.storeState['scrollPosition'];
        this.allDataListWithFilters = this.storeState['allDataListWithFilters'];
        this.record_Open_Azcordian = this.storeState['record_Open_Azcordian'];
        $("." + this.breadcumbProductCategory.toLowerCase()).prop("checked", true);
        //var self = this;
        // $(document).ready(function () {
        //     console.log(self.scrollPosition);
        //     $(this).scrollTop(self.scrollPosition);
        // });
        console.log(this.record_Open_Azcordian);
        let tempRecord = this.record_Open_Azcordian;
        for (let i = 0; i < tempRecord.length; i++) {
            setTimeout(function () {
                console.log(tempRecord)
                $('#activeContentFilter' + tempRecord[i]).addClass('activeContentA');
                $('#activeContentFilterli' + tempRecord[i]).addClass('active');
            }, 100)
        }
        this.userService.isSerachTerm.subscribe(searchText => {
            if (this.searchCount > 1) {
                this.breadcumbProductCategory = 'ALL';
                this.getAllProducts(searchText);
            }
            else {
                this.searchCount += 1;
            }
        });
        //this.prev_page_isDetail = false;

    }

}
