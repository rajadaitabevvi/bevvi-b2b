import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'calculateCartPrice'
})
export class CalculateCartPricePipe implements PipeTransform {

    transform(value: any, quantity: number): any {
        return parseFloat((value * quantity).toFixed(2))
    }

}
