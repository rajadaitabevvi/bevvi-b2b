export const environment = {
    production: true,
    googleAnalytics: {
        domain: 'auto', // 'none' for localhost           'auto' when going live
        // trackingId: 'UA-131564755-1' // DEV
        trackingId: 'UA-126994279-1' // PROD
    }
};
// CHECK GOOGLE ANALYTIC ID 131564755 DEV
// CHECK GOOGLE ANALYTIC ID 126994279 PROD
